/*
   db.h
  
   THIS SOFTWARE IS IN PUBLIC DOMAIN
     There is no limitation on any kind of usage of this code. 
     You use it because it is yours.
     
     Originally written and maintained by Matlib (matlib@matlibhax.com),
     any comments or suggestions are welcome.
  
   1  2018-07-24  Matlib
   2  2018-08-04  Matlib  db format change
*/




#ifndef _DB_H
#define _DB_H 1

#include <inttypes.h>
#include <unistd.h>

#define SUEX_GETGRP 1
#define SUEX_GETAUX 2
#define SUEX_HOME   4

struct DBData
{
	uid_t user;
	unsigned flags;
	unsigned groups_n;
	gid_t *groups;
	char *path;
	char *interpreter;
};

void db_read (int dbf, struct DBData **, unsigned *);

#endif
