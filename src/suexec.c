/*
   suexec.c
  
   THIS SOFTWARE IS IN PUBLIC DOMAIN
     There is no limitation on any kind of usage of this code. 
     You use it because it is yours.
     
     Originally written and maintained by Matlib (matlib@matlibhax.com),
     any comments or suggestions are welcome.
  
   1  2018-07-24  Matlib 
   2  2018-08-02  Matlib  add homedir capability
*/



#include <stdlib.h>
#include <stdio.h>
#include <syslog.h>
#include <fcntl.h>
#include <errno.h>
#include <string.h>
#include <strings.h>
#include <stdarg.h>
#include <fnmatch.h>
#include <unistd.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/file.h>
#include <pwd.h>
#include <grp.h>

#include "defaults.h"
#include "db.h"
#include "util.h"
#include "error.h"



void error (char const *const format, ...)
{
	va_list ap;
	va_start (ap, format);
	vsyslog (LOG_CRIT, format, ap);
	va_end (ap);
	exit (EXIT_FAILURE);
}



struct DBData *db;
unsigned entries;

int main (int const argc, char **const argv, char **const environ)
{
	unsigned i, j, k;
	char *p, *call_type;
	size_t l;
	struct DBData *def;
	struct passwd *user_info = NULL;
	char *p_cmd, **p_arg, *p_user, *p_group;

	openlog ("suexec", LOG_NDELAY | LOG_PID, LOG_AUTH);

	i = open (DBFILE, O_RDONLY);
	error_if (i == -1, "cannot open '%s': %s", DBFILE, strerror (errno));
	error_if (flock (i, LOCK_SH), "cannot lock '%s': %s", DBFILE,
		 strerror (errno));
	db_read (i, &db, &entries);
	close (i);



	// argc == 1
	// indicates that suexec has been started from ScriptAlias
	// real script name has to be extracted from $PATH_TRANSLATED

	if (argc == 1)
	{
		char *pt, *path_translated = getenv ("PATH_TRANSLATED");
		error_if (!path_translated, "PATH_TRANSLATED is not defined");
		error_if ((path_translated [0] != '/')
			 || !path_translated [1], "PATH_TRANSLATED is too short");

		call_type = "handler";
		p_user = p_group = NULL;
		pt = sstrdup (path_translated);

		setreuid (geteuid(), geteuid());
		PSEARCH:
		p_cmd = realpath (pt, NULL);
		if (!p_cmd)
		{
			if (errno == ENOTDIR)
			{
				p = rindex (pt, '/');
				error_if (!p || (p == pt), "unable to resolve path '%s'",
					 path_translated);
				*p = 0;
				free (p_cmd);
				goto PSEARCH;
			}
			error ("unable to resolve path '%s': %s", path_translated,
				 strerror (errno));
		}
		p = rindex (pt, '/');
		error_if (!p, "resolved path is invalid '%s'", path_translated);
		p [1] = 0;
		error_if (chdir (pt), "unable to change directory to '%s': %s", pt,
			 strerror (errno));
		free (pt);
		p_arg = smalloc (2 * sizeof (char *));
		p_arg [0] = p_cmd;
		p_arg [1] = 0;
	}

	// argc == 4
	// means that suexec has been called from mod_suexec and the three arguments
	// contain: user (ignored unless starts with ~ indicating home dir),
	// group (ignored), actual CGI script name

	else if (argc == 4)
	{
		call_type = "suexec";
		p_user = argv [1];
		p_group = argv [2];
		if (!*p_user || !*p_group || ((p_user [0] == '~') && !p_user [1]))
			error ("invalid user or group: '%s', '%s'", p_user, p_group);
		if (!*argv [3] || index (argv [3], '/'))
			error ("command contains incorrect path: '%s'", argv [3]);

		p_cmd = smalloc (256);
		p = getcwd (NULL, 0);
		error_if (!p, "cannot get current directory: %s", strerror (errno));
		int s = snprintf (p_cmd, 256, "%s/%s", p, argv [3]);
		error_if (s < 0, "cannot generate path: %s", strerror (errno));
		if (s >= 256)
		{
			free (p_cmd);
			p_cmd = malloc (s + 1);
			s = snprintf (p_cmd, s + 1, "%s/%s", p, argv [3]);
			error_if (s < 0, "cannot generate path: %s", strerror (errno));
		}
		p_arg = argv + 3;
	}

	else
		error ("called with wrong number of arguments (%d)", argc - 1);

	for (i = 0; (i < entries) && fnmatch (db [i].path, p_cmd, 0); i ++);
	error_if (i == entries, "no matching entry for '%s' found", p_cmd);
	def = db + i;

	if (def -> flags & SUEX_HOME)
	{
		if (!p_user)
		{
			struct stat st;
			error_if (stat (p_cmd, &st), "Cannot stat '%s': %s", p_cmd,
				 strerror (errno));
			def -> user = st.st_uid;
		}
		else
		{
			error_if (p_user [0] != '~',
				 "home directory must be called from UserDir");
			def -> user = strtol (++ p_user, &p, 10);
			error_if (*p, "invalid UID '%s'", p_user);
		}

		struct passwd *user_info = getpwuid (def -> user);
		error_if (!user_info, "cannot retrieve information for user '%s' for '%s'",
			 p_user, p_cmd);

		if (!p_user)
		{
			char *rp = realpath (user_info -> pw_dir, NULL);
			error_if (!rp, "cannot normalize user home directory '%s': %s",
				 user_info -> pw_dir, strerror (errno));
			l = strlen (rp);
			error_if ((strlen (p_cmd) <= l) || (p_cmd [l] != '/')
				 || strncmp (rp, p_cmd, l),
				 "script '%s' outside of user directory '%s'", p_cmd, rp);
			free (rp);
		}
	}

#ifdef UID_MIN
	error_if (def -> user < UID_MIN, "forbidden UID %d in definition %u for '%s'",
		 def -> user, i, p_cmd);
#endif
#ifdef GID_MIN
	for (j = 0; j < def -> groups_n; j ++)
		error_if (def -> groups [j] < GID_MIN,
			 "forbidden GID %d in definition %u for '%s'", def -> groups [j], i,
				 p_cmd);
#endif

	gid_t group;
	if (def -> flags & SUEX_GETGRP)
	{
		if (!user_info)
		{
			user_info = getpwuid (def -> user);
			error_if (!user_info, "cannot retrieve information for user %d for '%s'",
				 def -> user, p_cmd);
		}
		group = user_info -> pw_gid;
		if (def -> flags & SUEX_GETAUX)
			error_if (initgroups (user_info -> pw_name, group),
				 "cannot initialize supplementary groups: %s", strerror (errno));
		else
			error_if (setgroups (1, &group),
				 "cannot initialize supplementary groups: %s", strerror (errno));
	}
	else
	{
		error_if (!def -> groups_n, "invalid group definition %u for '%s'", i,
			 p_cmd);
		group = def -> groups [0];
		error_if (setgroups (def -> groups_n, def -> groups),
				 "cannot initialize supplementary groups: %s", strerror (errno));
	}

	error_if (setregid (group, group),
		 "cannot set group %d for '%s': %s", group, p_cmd, strerror (errno));
	error_if (setreuid (def -> user, def -> user),
		 "cannot set user %d for '%s': %s", def -> user, p_cmd, strerror (errno));

	for (i = 0; environ [i]; i ++);
	char **new_env = smalloc ((i + 1) * sizeof (char *));

	k = 0;
	for (i = 0; environ [i]; i ++)
		if (!strncmp (environ [i], "HTTP_", 5) || !strncmp (environ [i], "SSL_", 4))
			new_env [k ++] = environ [i];
		else for (j = 0; j < ALEN (SAFE_ENV); j ++)
		{
			l = strlen (SAFE_ENV [j]);
			if (!strncmp (environ [i], SAFE_ENV [j], l) && (environ [i][l] == '='))
			{
				new_env [k ++] = environ [i];
				break;
			}
		}
	new_env [k] = NULL;

	if (*def -> interpreter)
	{
		syslog (LOG_INFO, "%s launch '%s'->'%s' as %d/%d", call_type,
			 def -> interpreter, p_cmd, def -> user, group);
		execve (def -> interpreter, p_arg, new_env);
	}
	else
	{
		syslog (LOG_INFO, "%s launch '%s' as %d/%d", call_type, p_cmd,
			 def -> user, group);
		execve (p_cmd, p_arg, new_env);
	}

	error ("exec of '%s' failed: %s", p_cmd, strerror (errno));
	return EXIT_FAILURE;
}
