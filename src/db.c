/*
   db.c
  
   THIS SOFTWARE IS IN PUBLIC DOMAIN
     There is no limitation on any kind of usage of this code. 
     You use it because it is yours.
     
     Originally written and maintained by Matlib (matlib@matlibhax.com),
     any comments or suggestions are welcome.
  
   1  2018-07-24  Matlib 
   2  2018-08-04  Matlib  db format change
*/



#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <limits.h>

#include "defaults.h"
#include "db.h"
#include "util.h"
#include "error.h"


void db_read (int const dbf, struct DBData **const data,
	 unsigned *const entries)
{
	size_t dbf_size, dp;
	ssize_t r;
	void *d;
	unsigned e;

	dbf_size = lseek (dbf, 0, SEEK_END);
	error_if (dbf_size == -1, "cannot determine database size: %s", 
		 strerror (errno));
	lseek (dbf, 0, SEEK_SET);
	info (2, "database opened, size %lld", (long long) dbf_size);

	d = smalloc (dbf_size);
	dp = 0;
	do
	{
		r = read (dbf, d + dp, dbf_size - dp);
		error_if (r <= 0, "cannot read database: %s", r == -1 ? strerror (errno)
			 : "premature end of file");
		dp += r;
	}
	while (dp < dbf_size);

	error_if (((char *) d) [dbf_size - 1],
		 "database corrupted: data does not end with null character");

	*entries = *((unsigned *) d);
	*data = d + sizeof (unsigned);
	info (2, "%u entr%s", *entries, *entries == 1 ? "y" : "ies");
	error_if ((*entries >= MAX_ENTRIES)
		 || (*entries * sizeof (struct DBData) >= dbf_size),
		 "database corrupted: number of entries %u out of range", *entries);

	for (e = 0; e < *entries; e ++)
	{
		info (3, "  %u: flags %#x  UID %u  groups: %u", e, (*data) [e].flags,
			 (*data) [e].user, (*data) [e].groups_n);
		info (3, "    offsets: groups %lu  path %lu  interpreter %lu",
			 (unsigned long) (*data) [e].groups, (unsigned long) (*data) [e].path,
			 (unsigned long) (*data) [e].interpreter);

		error_if ((*data) [e].groups_n > MAX_GROUPS,
			 "database corrupted: too many groups %u", (*data) [e].groups_n);
		error_if (((size_t) (*data) [e].groups > dbf_size)
			 || ((size_t) ((*data) [e].groups + (*data) [e].groups_n) > dbf_size),
			 "database corrupted: pointer for groups is beyond end of file");
		(*data) [e].groups = d + (size_t) ((void *) (*data) [e].groups);
		error_if ((size_t) (*data) [e].path >= dbf_size,
			 "database corrupted: pointer for path is beyond end of file");
		(*data) [e].path += (size_t) d;
		error_if ((size_t) (*data) [e].interpreter >= dbf_size,
			 "database corrupted: pointer for interpreter is beyond end of file");
		(*data) [e].interpreter += (size_t) d;

		info (3, "    path '%s'", (*data) [e].path);
		info (3, "    interpreter '%s'", (*data) [e].interpreter);
	}

#ifndef NO_VERBOSE
	size_t str_end;
	dp = sizeof (unsigned);
	for (e = 0; e < *entries; e ++)
	{
		str_end = (size_t) (*data) [e].path - (size_t) (d)
			 + strlen ((*data) [e].path) + 1;
		if (str_end > dp)
			dp = str_end;
		str_end = (size_t) (*data) [e].interpreter - (size_t) (d)
			 + strlen ((*data) [e].interpreter) + 1;
		if (str_end > dp)
			dp = str_end;
	}
	error_if (dp > dbf_size, "internal error: db > dbf_size");
	if (dp < dbf_size)
		info (0, "database corrupted: extra data beyond end of table");
#endif
}
