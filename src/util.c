/*
   util.c
  
   THIS SOFTWARE IS IN PUBLIC DOMAIN
     There is no limitation on any kind of usage of this code. 
     You use it because it is yours.
     
     Originally written and maintained by Matlib (matlib@matlibhax.com),
     any comments or suggestions are welcome.
  
   2  2018-08-03  Matlib 
*/

#include <stdlib.h>
#include <string.h>
#include <errno.h>

#include "util.h"
#include "error.h"

void *smalloc (size_t const size)
{
	void *ptr = malloc (size);
	error_if (!ptr, "cannot allocate memory: %s", strerror (errno));
	memset (ptr, 0, size);
	return ptr;
}

char *sstrdup (char const *const s)
{
	char *ns = strdup (s);
	error_if (!ns, "cannot allocate new string: %s", strerror (errno));
	return ns;
}
