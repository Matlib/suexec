/*
   error.h
  
   THIS SOFTWARE IS IN PUBLIC DOMAIN
     There is no limitation on any kind of usage of this code. 
     You use it because it is yours.
     
     Originally written and maintained by Matlib (matlib@matlibhax.com),
     any comments or suggestions are welcome.
  
   1  2018-07-24  Matlib 
*/



#ifndef _ERROR_H
#define _ERROR_H 1

void
	 __attribute__ ((noreturn, format (printf, 1, 2)))
	 error (char const *, ...);

#define error_if(__COND__, ...) \
	do { \
		if (__COND__) \
			error (__VA_ARGS__); \
	} while (0)


#ifdef NO_VERBOSE
	#define info(...)
#else
	void
		 __attribute__ ((format (printf, 2, 3)))
		 info (int level, char const *, ...);
#endif

#endif
