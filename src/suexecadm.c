/*
   suexecadm.c
  
   THIS SOFTWARE IS IN PUBLIC DOMAIN
     There is no limitation on any kind of usage of this code. 
     You use it because it is yours.
     
     Originally written and maintained by Matlib (matlib@matlibhax.com),
     any comments or suggestions are welcome.
  
   1  2018-07-24  Matlib
   2  2018-08-04  Matlib  add homedir capability, db format change
   3  2020-09-15  Matlib  opening read-only
   4  2020-11-11  Matlib  fix arguments
*/



#include <stdlib.h>
#include <stdio.h>
#include <stdarg.h>
#include <errno.h>
#include <string.h>
#include <fcntl.h>
#include <sys/file.h>
#include <sys/types.h>
#include <pwd.h>
#include <grp.h>

#include <arguments.h>


#define PROGRAM "suexecadm"

#include "defaults.h"
#include "db.h"
#include "util.h"
#include "error.h"


struct Argument arguments [] =
{
	{ 'd', "db",      argument_string, ARGUMENT_F_PAR | ARGUMENT_F_ONCE, { .value_str = DBFILE }, "DB",  "the database to be worked on (default is " DBFILE ")" },
	{ 'p', "pos",     argument_int,    ARGUMENT_F_PAR | ARGUMENT_F_ONCE, { .value_int = 0 },      "NUM", "the entry number to be inserted or deleted\ncounting starts from 0" },
	{ 'v', "verbose", argument_bool,   0,                                { .value_u = 0 },        NULL,  "print informative messages\napplying this option more than once will cause lower and lower level messages to be printed" },
	{ 'A', "add",     argument_bool,   ARGUMENT_F_ONCE,                  { .value_u = 0 },        NULL,  "add a new entry specified in command line in position given with the -p parameter (default is to append at the end)" },
	{ 'C', "check",   argument_bool,   ARGUMENT_F_ONCE,                  { .value_u = 0 },        NULL,  "check database integrity only" },
	{ 'D', "del",     argument_bool,   ARGUMENT_F_ONCE,                  { .value_u = 0 },        NULL,  "delete one entry, either the one given with -p parameter or the one matching command line" },
	{ 'F', "flush",   argument_bool,   ARGUMENT_F_ONCE,                  { .value_u = 0 },        NULL,  "remove all entries from the database" },
	{ 'L', "list",    argument_bool,   ARGUMENT_F_ONCE,                  { .value_u = 0 },        NULL,  "list entries in the database in human readable format" },
	{ 0,   "dump",    argument_bool,   ARGUMENT_F_ONCE,                  { .value_u = 0 },        NULL,  "dump the database in text format" },
	{}
};

enum
{
	ARG_N_DB, ARG_N_POS, ARG_N_VERBOSE,
	ARG_N_ADD, ARG_N_CHECK, ARG_N_DEL, ARG_N_FLUSH, ARG_N_LIST, ARG_N_DUMP
};

#define ARG_DB      (arguments [ARG_N_DB].value_str)
#define ARG_POS     (arguments [ARG_N_POS].value_int)
#define ARG_POS_SET (arguments [ARG_N_POS].flags & ARGUMENT_F_OCC)
#define ARG_VERBOSE (arguments [ARG_N_VERBOSE].value_u)
#define ARG_ADD     (arguments [ARG_N_ADD].value_u)
#define ARG_CHECK   (arguments [ARG_N_CHECK].value_u)
#define ARG_DEL     (arguments [ARG_N_DEL].value_u)
#define ARG_FLUSH   (arguments [ARG_N_FLUSH].value_u)
#define ARG_LIST    (arguments [ARG_N_LIST].value_u)
#define ARG_DUMP    (arguments [ARG_N_DUMP].value_u)


void
	 __attribute__ ((noreturn, format (printf, 1, 2)))
	 error (char const *const format, ...)
{
	fprintf (stderr, PROGRAM " error: ");
	va_list ap;
	va_start (ap, format);
	vfprintf (stderr, format, ap);
	va_end (ap);
	fputc ('\n', stderr);
	exit (EXIT_FAILURE);
}

void info (int const level, char const *const format, ...)
{
	if (level > ARG_VERBOSE)
		return;
	fprintf (stderr, PROGRAM ": ");
	va_list ap;
	va_start (ap, format);
	vfprintf (stderr, format, ap);
	va_end (ap);
	fputc ('\n', stderr);
}



static void *write_record (int const fd, size_t *const ptr, void *const data,
	 size_t data_l)
{
	size_t ptr_start = 0;
	ssize_t r;
	if (ptr)
		ptr_start = *ptr;
	while (data_l)
	{
		r = write (fd, data, data_l);
		error_if (r == -1, "cannot write to database: %s", strerror (errno));
		if (ptr)
			(*ptr) += r;
		data_l -= r;
	}
	return (void *) ptr_start;
}

static void printq (char const *const str)
{
	size_t p, l = strlen (str);
	for (p = 0; p < l; p ++)
		if ((str [p] <= ' ') || (str [p] > '~') || (str [p] == '\\'))
			printf ("\\%03o", (unsigned char) str [p]);
		else
			putchar (str [p]);
}



int dbf;
struct DBData *db;
unsigned entries;

static void db_open ()
{
	int i, open_mode = O_RDONLY;
	unsigned e;

	if (ARG_ADD || ARG_DEL || ARG_FLUSH)
		open_mode = O_RDWR;

	dbf = open (ARG_DB, open_mode | O_NOCTTY);
	if ((dbf == -1) && (errno == ENOENT))
	{
		dbf = open (ARG_DB, O_RDWR | O_NOCTTY | O_CREAT | O_EXCL, 0666);
		error_if (dbf == -1, "cannot create new database '%s': %s", ARG_DB,
			 strerror (errno));
		e = 0;
		error_if (write (dbf, &e, sizeof (e)) != sizeof (e),
			 "cannot write to database file: %s", strerror (errno));
		error_if (lseek (dbf, 0, SEEK_SET), "Cannot seek on database file: %s",
			 strerror (errno));
		info (0, "created new database '%s'", ARG_DB);
	}
	error_if (dbf == -1, "cannot open '%s': %s", ARG_DB, strerror (errno));

	i = flock (dbf, LOCK_EX | LOCK_NB);
	if (i && (errno == EWOULDBLOCK))
	{
		info (1, "database file '%s' is locked, waiting", ARG_DB);
		i = flock (dbf, LOCK_EX);
	}
	error_if (i, "cannot lock database file '%s': %s", ARG_DB, strerror (errno));

	info (1, "database file '%s' opened", ARG_DB);
	db_read (dbf, &db, &entries);
}



int main (int argc, char **argv)
{
	unsigned i, j;
	int arg;
	char *t;
	struct DBData input;

	arg = arguments_parse (PROGRAM, VERSION,
		 "[pattern interpreter user group group ... ]",
		 "Manages the suexec database used by the suexec(8) CGI helper.",
		 argc, (void *) argv, arguments);
	if (!(ARG_ADD + ARG_CHECK + ARG_DEL + ARG_FLUSH + ARG_LIST + ARG_DUMP))
		arguments_error_freeform (
			 "Either command add, check, del, flush or list must be specified.");
	if (ARG_ADD + ARG_CHECK + ARG_DEL + ARG_FLUSH > 1)
		arguments_error_freeform (
			 "Only one of add, check, del, and flush may be specified.");
	if (ARG_DUMP + ARG_LIST > 1)
		arguments_error_freeform ("Please specify either list or dump.");

	memset (&input, 0, sizeof (input));
	if (arg < argc)
	{
		if (!(ARG_ADD + ARG_CHECK + ARG_DEL))
			arguments_error_freeform (
				 "Either add, check or del must be specified with a new definition.");

		if (argc - arg < 3)
			arguments_error_freeform ("Too few arguments given");
		if (!*argv [arg])
			arguments_error_freeform ("Pattern must not be empty");
		input.path = argv [arg ++];
		input.interpreter = argv [arg ++];
		if (!strcmp (input.interpreter, "-"))
			input.interpreter = "";

		if (!*argv [arg])
			arguments_error_freeform ("Empty user ID");
		if (strcmp (argv [arg], "~"))
		{
			input.user = strtol (argv [arg], &t, 0);
			if (*t)
			{
				struct passwd *p = getpwnam (argv [arg]);
				error_if (!p, "user '%s' not found", argv [arg]);
				input.user = p -> pw_uid;
				if (input.user < UID_MIN)
					info (0, "warning: UID %u lower than defined minimum %u",
						 input.user, UID_MIN);
			}
		}
		else
			input.flags |= SUEX_HOME;

		arg ++;

		if (arg == argc)
		{
			input.flags |= SUEX_GETGRP | SUEX_GETAUX;
			input.groups_n = 0;
		}
		else if ((arg == argc - 1) && !strcmp (argv [arg], "-"))
		{
			input.flags |= SUEX_GETGRP;
			input.groups_n = 0;
		}
		else
		{
			input.groups_n = argc - arg;
			if (input.groups_n > MAX_GROUPS)
				info (0, "warning: maximum number of groups %u exceeded", MAX_GROUPS);
			input.groups = smalloc (sizeof (gid_t) * input.groups_n);
			struct group *g;
			for (i = 0; arg < argc; i ++, arg ++)
			{
				input.groups [i] = strtol (argv [arg], &t, 0);
				if (*t)
				{
					g = getgrnam (argv [arg]);
					error_if (!g, "group '%s' not found", argv [arg]);
					input.groups [i] = g -> gr_gid;
				}
			}
		}

		if (ARG_VERBOSE >= 3)
		{
			char t [1024];
			memset (t, 0, sizeof (t));
			for (i = 0; i < input.groups_n; i ++)
				snprintf (t + strlen (t), sizeof (t) - 1 - strlen (t), " %d",
					 input.groups [i]);
			info (3, "input path pattern '%s' interpreter '%s'"
				 " flags%s%s%s%s user %d groups%s", input.path, input.interpreter,
				 !input.flags ? " none" : "",
				 input.flags & SUEX_GETGRP ? " getgrp" : "",
				 input.flags & SUEX_GETAUX ? " getaux" : "",
				 input.flags & SUEX_HOME ? " home" : "",
				 input.user, *t ? t : " (none)");
		}
	}

	db_open ();

	if (!ARG_POS_SET)
		ARG_POS = entries;
	else
	{
		if (((ARG_POS < 0) && (-ARG_POS > entries))
			 || ((ARG_POS > 0) && (ARG_POS >= entries)))
			arguments_error_freeform ("Position %ld exceeds number of entries %u.",
				 ARG_POS, entries);
		if (ARG_POS < 0)
			ARG_POS = entries + ARG_POS;
		info (3, "processing entry %ld", ARG_POS);
	}

	if (ARG_FLUSH)
	{
		info (2, "clearing %u entr%s", entries, entries == 1 ? "y" : "ies");
		entries = 0;
	}

	if (ARG_DEL)
	{
		if (!ARG_POS_SET && !input.path)
			arguments_error (arguments + ARG_N_DEL,
				 "needs either rule number of definition");
		if (ARG_POS_SET)
			i = ARG_POS;
		else
		{
			for (i = 0; i < entries; i ++)
			{
				if ((db [i].user != input.user) || (db [i].flags != input.flags)
					 || (db [i].groups_n != input.groups_n)
					 || strcmp (db [i].path, input.path)
					 || strcmp (db [i].interpreter, input.interpreter))
					continue;
				for (j = 0; (j < input.groups_n)
					 && (db [i].groups [j] == input.groups [j]); j ++);
				if (j == input.groups_n)
					break;
			}
			error_if (i == entries, "definition not found");
		}
		memmove (db + i, db + i + 1, sizeof (struct DBData) * (entries - i));
		entries --;
		info (1, "removed definition %u", i);
	}

	if (ARG_ADD)
	{
		if (!input.path)
			arguments_error (arguments + ARG_N_ADD, "needs rule definition");
		struct DBData *new_db = smalloc (sizeof (struct DBData) * (entries + 1));
		memcpy (new_db, db, sizeof (struct DBData) * ARG_POS);
		memcpy (new_db + ARG_POS, &input, sizeof (struct DBData));
		memcpy (new_db + ARG_POS + 1, db + ARG_POS,
			 sizeof (struct DBData) * (entries - ARG_POS));
		db = new_db;
		entries ++;
	}

	if (ARG_LIST)
	{
		char line [1024];
		unsigned cols [4];
		cols [2] = 1;
		struct passwd *p;
		struct group *g;
		memset (cols, 0, sizeof (cols));
		for (i = 0; i < entries; i ++)
		{
			snprintf (line, sizeof (line), "%d", i);
			if (strlen (line) > cols [0])
				cols [0] = strlen (line);
			if (strlen (db [i].path) > cols [1])
				cols [1] = strlen (db [i].path);
			if (strlen (db [i].interpreter) > cols [2])
				cols [2] = strlen (db [i].interpreter);
			if (db [i].flags & SUEX_HOME)
				strcpy (line, "~");
			else
			{
				p = getpwuid (db [i].user);
				if (p)
					snprintf (line, sizeof (line), "%s(%d)", p -> pw_name, p -> pw_uid);
				else
					snprintf (line, sizeof (line), "%d", db [i].user);
			}
			if (strlen (line) > cols [3])
				cols [3] = strlen (line);
		}

		for (i = 0; i < entries; i ++)
		{
			if (db [i].flags & SUEX_HOME)
				strcpy (line, "~");
			else
			{
				p = getpwuid (db [i].user);
				if (p)
					snprintf (line, sizeof (line), "%s(%d)", p -> pw_name, p -> pw_uid);
				else
					snprintf (line, sizeof (line), "%d", db [i].user);
			}
			printf ("%-*d  %-*s  %-*s  %-*s %s", cols [0], i, cols [1],
				 db [i].path, cols [2],
				 strlen (db [i].interpreter) ? db [i].interpreter : "-",
				 cols [3], line, db [i].flags & SUEX_GETGRP ? " (@)" : "");
			for (j = 0; j < db [i].groups_n; j ++)
			{
				g = getgrgid (db [i].groups [j]);
				if (g)
					printf (" %s(%d)", g -> gr_name, g -> gr_gid);
				else
					printf (" %d", db [i].groups [j]);
			}
			if (db [i].flags & SUEX_GETAUX)
				 fputs (" (+)", stdout);
			putchar ('\n');
		}
	}

	if (ARG_DUMP)
	{
		struct passwd *p;
		struct group *g;
		for (i = 0; i < entries; i ++)
		{
			printq (db [i].path);
			putchar (' ');
			printq (*db [i].interpreter ? db [i].interpreter : "-");
			if (db [i].flags & SUEX_HOME)
				printf (" ~");
			else
			{
				p = getpwuid (db [i].user);
				if (p)
					printf (" %s", p -> pw_name);
				else
					printf (" %u", db [i].user);
			}
			if (db [i].flags & SUEX_GETGRP)
			{
				if (!(db [i].flags & SUEX_GETAUX))
					printf (" -");
			}
			else
				for (j = 0; j < db [i].groups_n; j ++)
				{
					g = getgrgid (db [i].groups [j]);
					if (g)
						printf (" %s", g -> gr_name);
					else
						printf (" %u", db [i].groups [j]);
				}
			putchar ('\n');
		}
	}

	if (ARG_ADD | ARG_DEL | ARG_FLUSH)
	{
		size_t dp = 0;

		error_if (lseek (dbf, 0, SEEK_SET) || ftruncate (dbf, 0),
			 "cannot reset database: %s", strerror (errno));
		write_record (dbf, &dp, &entries, sizeof (entries));
		write_record (dbf, &dp, db, sizeof (struct DBData) * entries);

		for (i = 0; i < entries; i ++)
		{
			db [i].groups = write_record (dbf, &dp, db [i].groups,
				 db [i].groups_n * sizeof (gid_t));
			db [i].path = write_record (dbf, &dp, db [i].path,
				 strlen (db [i].path) + 1);
			db [i].interpreter = write_record (dbf, &dp, db [i].interpreter,
				 strlen (db [i].interpreter) + 1);
		}

		error_if (lseek (dbf, sizeof (entries), SEEK_SET) == -1,
			 "cannot seek in database file: %s", strerror (errno));
		write_record (dbf, NULL, db, sizeof (struct DBData) * entries);

		info (2, "%u definition%s written", entries, entries == 1 ? "" : "s");
	}

	return EXIT_SUCCESS;
}
