/*
   util.h
  
   THIS SOFTWARE IS IN PUBLIC DOMAIN
     There is no limitation on any kind of usage of this code. 
     You use it because it is yours.
     
     Originally written and maintained by Matlib (matlib@matlibhax.com),
     any comments or suggestions are welcome.
  
   2  2018-08-03  Matlib 
*/

#ifndef _SMALLOC_H
#define _SMALLOC_H 1

#define ALEN(__ARRAY__) (sizeof (__ARRAY__) / sizeof ((__ARRAY__ [0])))

void *smalloc (size_t);
char *sstrdup (char const *);

#endif
