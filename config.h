/*
   Configuration file for suexec and suexecadm.
   The values can be changed here, or by adding to make command, i.e.

       make help DBFILE=/usr/local/etc/suexec.db
       make all DBFILE=/usr/local/etc/suexec.db UID_MIN=1000

*/

/*
   Database file that is opened at runtime. It is hardcoded as there is
   currently no way to specify this when starting. Could be done with
   environment variables though.
*/

// #define DBFILE "/etc/suexec.db"



/*
   Minimum allowed user and group IDs checked at runtime.
*/

// #define UID_MIN     100
// #define GID_MIN     20



/*
   Maximum number of definitions the database may have. By default the
   limit is architecture dependent.
*/

// #define MAX_ENTRIES (INT_MAX / sizeof (struct DBData))



/*
   Maximum number of auxiliary groups that a definition may have.
*/

// #define MAX_GROUPS  256
