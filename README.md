# What it is

This release of suexec is drop-in replacement for the original program bundled with Apache's httpd with much improved capabilities:

* user and group and not fixed to the virtual host configuration - credentials are read from a separate global configuration database and applied per directory and executed file,
* can be used from the mod_suexec module, or as a handler,
* supports home directories,
* supports loading auxiliary groups,
* does not require to be launched with user ID equal to 0 and supports capabilities/privileges,
* may execute scripts directly or call a predefined handler.

# Prerequisites

The *suexecadm* component requires [libarguments](https://matlibhax.com/~matlib/hacks/libarguments/)

# Compiling

1. Edit and adjust the config.h file.
2. Run `make help` to verify the changes. Make takes additional parameters that affect the building process:
  * **CC** defines the C compiler to use. This is usually defined by the make program and falls back to *gcc*. It will probably not compile with the default *c99* compiler due to missing calls in the C-99 standard.
  * **CFLAGS** specifies the compiler command line arguments and default to *-Wall -s -O2* unless overriden by command line or make internal settings.
3. After compiling the binaries are ready in the *bin* directory.

# Installing

Copy *bin/* to the desired installation root. Set privileges on *suexec*.

The database will be initialized the first time you use *suexecadm*. If you need to create it in a different location, then specify the path with the **-d** parameter:

```
$ suexecadm -Ld /tmp/new_database.db
```

# Using

The manual page has detailed information on using this flavour of suexec. The following commands provide a quick introduction to how to add rules to the database:

```
$ suexecadm -AL '/var/www/*.cgi' - wwwapp nogroup
0  /var/www/*.cgi  -  wwwapp(10000)  nogroup(65534)

$ suexecadm -AL '/var/www/*.php' /usr/bin/php-cgi wwwapp nogroup
0  /var/www/*.cgi  -                 wwwapp(10000)  nogroup(65534)
1  /var/www/*.php  /usr/bin/php-cgi  wwwapp(10000)  nogroup(65534)

$ suexecadm -AL '/var/home/*/public_html/*.cgi' - '~' -
0  /var/www/*.cgi                 -                 wwwapp(10000)  nogroup(655
1  /var/www/*.php                 /usr/bin/php-cgi  wwwapp(10000)  nogroup(655
2  /var/home/*/public_html/*.cgi  -                 ~              (@)
```
