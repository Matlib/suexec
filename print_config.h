#include "defaults.h"

#ifdef DBFILE
@"Default database:    " DBFILE
#endif

#ifdef UID_MIN
@"Minimum UID:         " UID_MIN
#endif

#ifdef GID_MIN
@"Minimum GID:         " GID_MIN
#endif

#ifdef MAX_ENTRIES
@"Max number of rules: " GID_MIN
#endif

#ifdef MAX_GROUPS
@"Max number of groups:" GID_MIN
#endif
