.POSIX:
CC?=gcc
CFLAGS?=-Wall -s -O2

SE=bin/suexec
SEA=bin/suexecadm

all: $(SE) $(SEA)
	@echo
	@echo "Build complete in bin/"
	@echo
	@ls -Al bin
	@echo

help:
	@echo
	@echo "Available targets:"
	@echo "  all"
	@echo "  clean"
	@echo " " $(SE)
	@echo " " $(SEA)
	@echo
	@echo "Defaults:"
	@echo "  CC      = $(CC)"
	@echo "  CFLAGS  = $(CFLAGS)"
	@echo "  LDFLAGS = $(LDFLAGS)"
	@echo
	@echo "Configuration (config.h):"
	@$(CC) -Isrc -I. -E print_config.h | sed -ne '/^@/s/^@"\([^"]*\)"/  \1 /p'
	@echo

clean:
	rm -fr bin

$(SE): config.h src/db.c src/db.h src/error.h src/suexec.c src/util.c  src/util.h
	mkdir -p bin
	$(CC) $(CFLAGS) -o $(SE) -DNO_VERBOSE=1 -I. $(LDFLAGS) src/suexec.c src/util.c src/db.c

$(SEA): config.h src/db.c src/db.h src/error.h src/suexecadm.c src/util.c  src/util.h
	mkdir -p bin
	$(CC) $(CFLAGS) -o $(SEA) -I. $(LDFLAGS) src/suexecadm.c src/util.c src/db.c -larguments
