.Dd August 22, 2018
.Dt suexec 8
.Sh NAME
.Nm suexec
.Nd switch to another user when executing CGI script
.Sh SYNOPSIS
.Pp
With mod_suexec for each virtual host:
.Pp
.Cd SuexecUserGroup Ar user group
.Cd AddHandler cgi-script Ar file-mask
.Pp
As a handler:
.Pp
.Cd Action suexec /suexec
.Cd ScriptAlias /suexec Ar path-to-suexec
.Cd AddHandler suexec Ar file-mask
.Sh DESCRIPTION
The
.Nm suexec
handler allows executing CGI scripts with credentials other that the calling
HTTP daemon. This flavour of suexec has several improvements over the original
Apache's implementation:
.Bl -bullet
.It
one global configuration for users and groups based on the path executed
instead of fixed per virtual host setting in the
.Xr httpd 8
config files,
.It
can be used as direct suexec replacement or as a handler with home directory
support in both modes,
.It
supports loading group and auxiliary groups either from its own definitions or
determined from system configuration,
.It
does not require to be launched as SUID with user ID equal to 0,
.It
may execute scripts directly or call a defined handler,
.It
writes meaningful messages to syslog's
.Em auth
facility.
.El
.Pp
Credentials are stored in one binary database location of which is defined at
compile time. The database is administered with the
.Xr suexecadm 8
utility. Suexec processes the rules one by one and executes the CGI script
based on the first matching entry. It exits with error if no rules match.
.Sh USAGE
.Ss Configuring httpd
This program may be used as direct drop-in replacement over the original
Apache's suexec or as a handler. If used as a replacement please follow
the standard
.Em mod_suexec
deployment instructions from the httpd project. Please note
that the
.Cd SuexecUserGroup
directive must be provided to activate suexec script execution but the actual
user and group values are ignored as the external database is used to determine
credentials.
.Pp
When using it as a handler four modules have to be imported:
.Em mod_cgi , mod_actions , mod_alias ,
and
.Em mod_mime .
.Pp
The
.Pa suexec
binary must pass httpd's ordinary security checks, that is it must reside
in a location allowed to be browsed by the client. It is sufficient to
create symlink in
.Cd DocumentRoot
leading to the actual binary combined with the
.Ar FollowSymLinks
and
.Ar ExecCGI
options for the directory.
.Pp
Once the binary is installed and available it must added as a handler for
CGI scripts with the following directives:
.Bl -item -compact -offset indent
.It
.Cd Action
suexec /suexec
.It
.Cd ScriptAlias /suexec
.Ar suexec-full-path
.It
.Cd AddHandler suexec
.Ar suffix
.El
.Pp
An example configuration for document root
.Pa /var/www
containing symlink to the suexec binary looks like this:
.Bd -literal -offset 4m
DocumentRoot "/var/www"
<Directory "/var/www">
  Options Indexes FollowSymLinks ExecCGI
  AllowOverride None
  Require all granted
</Directory>
Action suexec /suexec
ScriptAlias /suexec /var/www/suexec
AddHandler suexec .cgi
AddHandler suexec .php
.Ed
.Pp
Once configured it starts logging successes and failures to syslog's
.Em auth
facility.
.Ss Adding rules
Credentials are set up using the
.Xr suexecadm 8
utility that is responsible for maintaining the path and user database.
The database is initialised automatically on first use. It may be also put
in a different location when the calling user does not have permissions to
create the file:
.Bd -literal -offset 4m
suexecadm -L -d /tmp/suexec.db
.Ed
.Pp
The rules listed with the
.Fl L
option contain the following columns:
.Bl -enum
.It
rule number, starting with 0
.It
path mask using the
.Xr fnmatch 3
syntax (the so called globbing),
.It
optional handler to be called against the script; dash indicates that no
handler shall be used and the script should be called directly,
.It
execution user; the tilde
.Ql ~
indicates that this is user's home directory and the script should to be
launched with that user's credentials; execution will fail if the absolute path
is outside of the home directory provided by the operating system,
.It
execution groups; the symbol
.Ql @
indicates that the group will be obtained from the system namespaces, and the
.Ql +
that the auxiliary groups will be attached as well.
.El
.Pp
Considering the following example setup:
.Bd -literal -offset 4m
0 /var/www/*.cgi            -                app(100) nogroup(65534)
1 /var/www/*.php            /usr/bin/php-cgi app(100) nogroup(65534)
2 /home/*/public_html/*.cgi -                ~        (@)
.Ed
.Pp
All scripts with suffix
.Em ".cgi"
residing in
.Pa /var/www
and all subdirectories will be called directly with user
.Em app
and one group
.Em nogroup .
.Pp
All PHP scripts with suffix
.Em ".php"
under
.Pa /var/www
and all subdirectories will cause
.Pa /usr/bin/php-cgi
to be called with appropriate environment with credentials same as above.
.Pp
All scripts ending with
.Em ".cgi"
in home directories will be called directly with the credentials of the
directory owner with its main group and no auxiliary groups.
.Sh SEE ALSO
.Xr httpd 8 ,
.Xr suexecadm 8
.Sh HISTORY
The work on this variant of suexec was started by Matlib around 2010. After
migrating to the binary database the version 1 was finally released in 2018.
.Sh AUTHORS
.An Matlib Ad matlib@matlibhax.com
